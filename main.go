package main

import (
	"flag"
	"fmt"
	"io/ioutil"
	"os"
	"time"

	"gopkg.in/gomail.v2"
)

type Amazon struct {
	From string
	to   string
	User string
	Pass string
	Smpt string
	Port int
}

func main() {

	emailto := flag.String("to", "", "Um email Válido")
	from := flag.String("from", "no-reply@trend2b.com", "Remetente")
	subject := flag.String("subject", "", "Assunto do Email")
	body := flag.String("body", "", "Corpo do Email/HTML")
	user := flag.String("user", "AKIAJUMA7VWVD5X4BGPA", "Usuário SMTP")
	pass := flag.String("pass", "Aq4K5uVO/NsinI/ZWsGtrHeYZO3/n5CKrIX2e0vzwEA6", "Senha SMTP")
	smtp := flag.String("smtp", "email-smtp.us-east-1.amazonaws.com", "Servidor SMTP")
	port := flag.Int("port", 587, "Porta SMTP")
	bodyfile := flag.String("bodyfile", "", "bodyfile que contêm o HTML")
	data := flag.String("data", "", "Dados para serem enviado no email")

	flag.Parse()

	/*
		if strings.Contains(*data, "|") {
			s := strings.Split(*data, "|")
		}*/

	if *emailto == "" || *subject == "" {
		flag.Usage()
		os.Exit(0)
	}

	if *bodyfile != "" {
		b, err := ioutil.ReadFile(*bodyfile)
		if err != nil {
			fmt.Println("Erro :", err)
			os.Exit(0)
		}
		*body = string(b)

		if *data != "" {
			*body = fmt.Sprintf(*body, *data)
		}

	}

	am := Amazon{*from, *emailto, *user, *pass, *smtp, *port}

	m := gomail.NewMessage()
	m.SetHeader("From", am.From)
	m.SetHeader("To", am.to)
	m.SetHeader("Subject", *subject)
	m.SetBody("text/html", *body)

	start := time.Now()
	d := gomail.NewDialer(am.Smpt, am.Port, am.User, am.Pass)
	if err := d.DialAndSend(m); err != nil {
		panic(err)
	} else {
		elapsed := time.Since(start)
		fmt.Printf("%s", elapsed)
	}

}
